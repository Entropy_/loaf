package loaf

import (
	"bufio"
	"fmt"
	"math"
	"strings"
	"os"
	"strconv"

	"github.com/wayneashleyberry/terminal-dimensions"
)

//Bread is usually collected into arrays with a string label, dirty flag and its
//position in the 2d array
type Bread struct {
	X, Y  int
	Label string
	Nl    bool
	Dirty bool
}

//Loaf stores the height and width of a toast
type Loaf struct {
	Height, Width int
}

//Flour is the interface for all flour instances
type Flour interface {
	Dough()
	Toast()
	Oven()
	Bread()
}

//SpawnIndex writes out a file to toast.
func SpawnIndex(path string, xvar int, yvar int, testToast []Bread, xlen int, yhei int) {
	//flour.ToastLogger("spawnIndex")
	filo, err := os.Open(path)
	filscan := bufio.NewScanner(filo)
	for filscan.Scan() {
		yvar++
		CopyToast(filscan.Text(), xvar, yvar, 1, testToast)

	}

	if err != nil {
		fmt.Println("Something went wrong!")
	}
	//return slice

}

//SpawnContents writes out a file to toast
func SpawnContents(path string, xvar int, yvar int, testToast []Bread) {
	//flour.ToastLogger("spawnContents")
	filo, err := os.Open(path)
	filscan := bufio.NewScanner(filo)
	for filscan.Scan() {
		yvar++
		CopyToast(filscan.Text(), xvar, yvar, 1, testToast)

	}

	if err != nil {
		//fmt.Println("Something went wrong!")
	}
}

//Flat clears the screen with the character provided
func Flat(label string, testToast []Bread) {
	//flour.ToastLogger("flat")
	for i := range testToast {
		testToast[i].Label = label
		testToast[i].Dirty = true
		//flour.CopyToast(label, testToast[i].X, testToast[i].Y, testToast[i].Y+1, testToast)

	}
	Toast(testToast, "green", "black")
	//fmt.Printf("\n<:o.o:>")
}

//CleanFlecks ranges over a loaf and if it is dirty, writes "_" to it
//and clears the dirty flag
func CleanFlecks(loaf []Bread) []Bread {
	//ToastLogger("CleanFlecks")
	for i := range loaf {
		if loaf[i].Label != "_" {
			loaf[i].Dirty = false
		}
	}
	return loaf
}

//MakeCleanFlecks ranges over a loaf and if it is dirty, writes "_" to it
//and clears the dirty flag
func MakeCleanFlecks(loaf []Bread) []Bread {
	//ToastLogger("MakeCleanFlecks")
	for i := range loaf {
		if loaf[i].Dirty == true {
			loaf[i].Label = "_"
			loaf[i].Dirty = false
		}
	}
	return loaf
}

//SpawnWin creates an array of a bread loaf with capacity and length xvar, yvar
func SpawnWin(xvar int, yvar int) ([]Bread, Loaf) {
	//ToastLogger("SpawnWin")
	var winLoaf Loaf
	winLoaf.Height = yvar
	winLoaf.Width = xvar
	win := Dough(xvar, yvar)
	win = Oven(win, "*", xvar, yvar)
	return win, winLoaf
}

//RelWin Copies a window with size and height relative to the size of the toast
//passed to the Loaf passed, or if the last value is false, the relative size of
//the terminal in which it is called.
func RelWin(widthP float64, heightP float64, heightPadding float64, widthPadding float64, win []Bread, testToast []Bread, winLoaf Loaf, relativeToParent bool) ([]Bread, Loaf) {

	var tHeight64 float64
	var tWidth64 float64
	if !relativeToParent {
		tHeight, err := terminaldimensions.Height()
		tWidth, err := terminaldimensions.Width()
		if err != nil {
			fmt.Println("terminal sizing error!")

		}
		tHeight64 = float64(tHeight)
		tWidth64 = float64(tWidth)

	} else {
		tHeight := winLoaf.Height
		tWidth := winLoaf.Width
		tHeight64 = float64(tHeight)
		tWidth64 = float64(tWidth)

	}

	xbeg := math.Floor((tWidth64 * widthP))
	xend := math.Floor(xbeg + widthPadding)

	ybeg := math.Floor((tHeight64 * heightP))
	yend := math.Floor(ybeg + heightPadding)

	xendI := int(xend)
	yendI := int(yend)
	ybegI := int(ybeg)
	xbegI := int(xbeg)
	for i := range win {
		CopySubToast(win[i].Label, win[i].X, win[i].Y, xbegI, xendI, ybegI, yendI, testToast)
	}

	return testToast, winLoaf
}

//CopySubToast copies the string passed into the values of a []Bread given
func CopySubToast(welcome string, xvar int, yvar int, xbeg int, xend int, ybeg int, yend int, testToast []Bread) []Bread {
	//ToastLogger("CopyToast")
	//wel := strings.Split(welcome, "")
	if yend != 0 {
		for x := ybeg - int(float64(yvar)*0.5); x < yend-int(float64(yvar)*0.5); x++ {
			for i := xbeg - int(float64(xvar)*0.5); i < xend-int(float64(xvar)*0.5); i++ {
				//                           DO STUFF HERE
				//			if x < yend && x > yend {
				//slice := BreadGetter(0, 0, testToast)
				slice := BreadGetter(xvar+i, yvar+x, testToast)
				slice.Label = welcome
				slice.Dirty = true
				testToast = BreadSetter(xvar+i, yvar+x, testToast, slice)
				//			}
			}
		}
	}
	return testToast
}

func CopyInToast(toast []Bread, testToast []Bread) []Bread {
	for i := range toast {
		testToast = CopyToast(toast[i].Label, toast[i].X, toast[i].Y, 1, testToast)
	}
	return testToast
}

//CopyColourToast copies the string passed into the values of a []Bread given
func CopyColourToast(welcome string, xvar int, yvar int, yend int, fore string, back string, testToast []Bread) []Bread {
	//ToastLogger("CopyColourToast")
	wel := strings.Split(welcome, "")
	if yend != 0 {
		for x := yend; x > 0; x-- {
			for i := 0; i < len(welcome); i++ {
				//                                        DO STUFF HERE
				slice := BreadGetter(xvar+i, yvar-x, testToast)
				slice.Label = Dye(string(wel[i]), fore, back)
				slice.Dirty = true
				testToast = BreadSetter(xvar+i, yvar-x, testToast, slice)
			}
		}
	}
	return testToast
}

//CopyToast copies the string passed into the values of a []Bread given
func CopyToast(welcome string, xvar int, yvar int, yend int, testToast []Bread) []Bread {
	//ToastLogger("CopyToast")
	wel := strings.Split(welcome, "")
	if yend != 0 {
		for x := yend; x > 0; x-- {
			for i := 0; i < len(welcome); i++ {
				//                                        DO STUFF HERE
				slice := BreadGetter(xvar+i, yvar-x, testToast)
				slice.Label = string(wel[i])
				slice.Dirty = true
				testToast = BreadSetter(xvar+i, yvar-x, testToast, slice)
			}
		}
	}
	return testToast
}

//Copy256Toast copies the string passed into the values of a []Bread given
func Copy256Toast(welcome string, xvar int, yvar int, yend int, r string, g string, b string, a string, fore bool, back bool, testToast []Bread) []Bread {
	//ToastLogger("CopyToast")
	wel := strings.Split(welcome, "")
	if yend != 0 {
		for x := yend; x > 0; x-- {
			for i := 0; i < len(welcome); i++ {
				//                                        DO STUFF HERE
				slice := BreadGetter(xvar+i, yvar-x, testToast)
				slice.Label = Dye256(string(wel[i]), r, g, b, fore, back)
				slice.Dirty = true
				testToast = BreadSetter(xvar+i, yvar-x, testToast, slice)
			}
		}
	}
	return testToast
}

//Dye256 Colours the string passed via the RGB values given and returns it
func Dye256(words string, r string, g string, b string, fore bool, back bool) string {
	var word string
	if fore == true {
		word = fmt.Sprint("\x1b[38;2;" + r + ";" + g + ";" + b + "m" + words + "\x1b[0m")
	}
	if back == true {
		word = fmt.Sprint("\x1b[48;2;" + r + ";" + g + ";" + b + "m" + words + "\x1b[0m")
	}
	//	words = fmt.Sprint("\x1b[38;" + r + ";" + g + ";" + b + ";" + a + "m" + words + "\x1b[0m")

	return word
}

//Dye dyes everything blue for now.
func Dye(words string, foreColour string, backColour string) string {
	//	colour := colornames.Aqua
	//	r, g, b := colour.R, colour.G, colour.B
	var word string
	switch foreColour {
	case "black":
		word = fmt.Sprint("\x1b[38;30m" + words + "\x1b[0m")
	case "red":
		word = fmt.Sprint("\x1b[38;31m" + words + "\x1b[0m")
	case "green":
		word = fmt.Sprint("\x1b[38;32m" + words + "\x1b[0m")
	case "yellow":
		word = fmt.Sprint("\x1b[38;33m" + words + "\x1b[0m")
	case "blue":
		word = fmt.Sprint("\x1b[38;34m" + words + "\x1b[0m")
	case "magenta":
		word = fmt.Sprint("\x1b[38;35m" + words + "\x1b[0m")
	case "cyan":
		word = fmt.Sprint("\x1b[38;36m" + words + "\x1b[0m")
	case "white":
		word = fmt.Sprint("\x1b[38;37m" + words + "\x1b[0m")
	case "brightblack":
		word = fmt.Sprint("\x1b[38;90m" + words + "\x1b[0m")
	case "brightred":
		word = fmt.Sprint("\x1b[38;91m" + words + "\x1b[0m")
	case "brightgreen":
		word = fmt.Sprint("\x1b[38;92m" + words + "\x1b[0m")
	case "brightyellow":
		word = fmt.Sprint("\x1b[38;93m" + words + "\x1b[0m")
	case "brightblue":
		word = fmt.Sprint("\x1b[38;94m" + words + "\x1b[0m")
	case "brightmagenta":
		word = fmt.Sprint("\x1b[38;95m" + words + "\x1b[0m")
	case "brightcyan":
		word = fmt.Sprint("\x1b[38;96m" + words + "\x1b[0m")
	case "brightwhite":
		word = fmt.Sprint("\x1b[38;97m" + words + "\x1b[0m")
	default:
		word = words
	}
	switch backColour {
	case "black":
		word = fmt.Sprint("\x1b[40m" + word + "\x1b[0m")
	case "red":
		word = fmt.Sprint("\x1b[41m" + word + "\x1b[0m")
	case "green":
		word = fmt.Sprint("\x1b[42m" + word + "\x1b[0m")
	case "yellow":
		word = fmt.Sprint("\x1b[43m" + word + "\x1b[0m")
	case "blue":
		word = fmt.Sprint("\x1b[44m" + word + "\x1b[0m")
	case "magenta":
		word = fmt.Sprint("\x1b[45m" + word + "\x1b[0m")
	case "cyan":
		word = fmt.Sprint("\x1b[46m" + word + "\x1b[0m")
	case "white":
		word = fmt.Sprint("\x1b[47m" + word + "\x1b[0m")
	case "brightblack":
		word = fmt.Sprint("\x1b[100m" + word + "\x1b[0m")
	case "brightred":
		word = fmt.Sprint("\x1b[101m" + word + "\x1b[0m")
	case "brightgreen":
		word = fmt.Sprint("\x1b[102m" + word + "\x1b[0m")
	case "brightyellow":
		word = fmt.Sprint("\x1b[103m" + word + "\x1b[0m")
	case "brightblue":
		word = fmt.Sprint("\x1b[104m" + word + "\x1b[0m")
	case "brightmagenta":
		word = fmt.Sprint("\x1b[105m" + word + "\x1b[0m")
	case "brightcyan":
		word = fmt.Sprint("\x1b[106m" + word + "\x1b[0m")
	case "brightwhite":
		word = fmt.Sprint("\x1b[107m" + word + "\x1b[0m")
	default:
		word = word
	}

	return word
}

//PrepareToast is the generalized print to screen function, similar to Blit in pygame
//or other visual based systems, rather than output the value calculated though, PrepareToast
//returns it as a string.
func PrepareToast(loaf []Bread, foreColour string, backColour string) string {
	//ToastLogger("Toast")
	var displaytoast string
	for i := range loaf {
		if loaf[i].Dirty {
			displaytoast += Dye(Fleck(i, loaf), foreColour, backColour)
		}
		if loaf[i].Dirty != true && loaf[i].Label != "_" {
			loaf[i].Label = "_"
			displaytoast += Dye(Fleck(i, loaf), foreColour, backColour)
		} else {
			//do nothing
		}
	}
//	displaytoast += "_\n\x1b[93;41m\x1b[3;6H<:o.o:>\x1b[0m"
	return displaytoast
}

//Toast is the generalized print to screen function, similar to Blit in pygame
//or other visual based systems.
func Toast(loaf []Bread, foreColour string, backColour string) {
	//ToastLogger("Toast")
	var displaytoast string
	for i := range loaf {
		if loaf[i].Dirty {
			displaytoast += Dye(Fleck(i, loaf), foreColour, backColour)
		}
		if loaf[i].Dirty != true && loaf[i].Label != "_" {
			loaf[i].Label = "_"
			displaytoast += Dye(Fleck(i, loaf), foreColour, backColour)
		} else {
			//do nothing
		}
	}
//	displaytoast += "_\n\x1b[93;41m\x1b[3;6H<:o.o:>\x1b[0m"
	fmt.Printf(displaytoast)
}

//Fleck copies the character in the loaf to the screen in place
func Fleck(index int, loaf []Bread) string {
	//ToastLogger("Fleck")
	text := fmt.Sprint("\x1b[", loaf[index].Y, ";", loaf[index].X, "H", loaf[index].Label, "\x1b[0m")
	return text
}

//Fleck does the same thing as Fleck, but prints it out rather than returning it to be
//printed later
func PrintFleck(index int, loaf []Bread) {
	//ToastLogger("PrintFleck")
	text := fmt.Sprint("\x1b[", loaf[index].Y, ";", loaf[index].X, "H", loaf[index].Label, "\x1b[0m")
	fmt.Printf(text)
}

//Oven initilizes an array breadloaf with default values
func Oven(butt []Bread, label string, xvar int, yvar int) []Bread {
	//ToastLogger("Oven")
	x := 0
	y := 0
	for index := range butt {

		//X values
		if x+1 == xvar {
			x = 0
			//			butt[index].Y = y
			butt[index].Nl = true
			y++
		}
		butt[index].X = x
		x++
		//Y values
		//		if index % yvar == 0{
		//			y++
		//		}
		butt[index].Y = y
	}
	var butter []Bread
	//we get some extraneous values
	for yandex := range butt {

		if label == "_" {
			if yandex%2 == 0 {
				butt[yandex].Label = "0"
			} else {
				butt[yandex].Label = "1"
			}
		} else {
			butt[yandex].Label = label
		}
		if butt[yandex].Y >= yvar {
			butter = butt[:yandex]
			//fmt.Println("GAME OVER MAN, GAME OVER")
			break
		}
	}
	return butter
}

func MoveTo(x int, y int) string {
	X := strconv.Itoa(x)
	Y := strconv.Itoa(y)
	moved := "\x1b["+Y+";"+X+"H"
	return moved
}

//BreadGetter grabs the slice of toast from an array of Bread and returns it
func BreadGetter(x int, y int, loaf []Bread) Bread {
	//ToastLogger("BreadGetter")
	//Gets the bread at position x, y
	var val Bread

	for i := range loaf {
		if loaf[i].Y == y {
			if loaf[i].X == x {
				val = loaf[i]
				break
			}
		}
	}
	return val
}

//BreadSetter sets the given value of a slice of Bread at points x and y
func BreadSetter(x int, y int, loaf []Bread, val Bread) []Bread {
	//ToastLogger("BreadSetter")
	//sets the Bread at position x, y
	for i := range loaf {
		if loaf[i].Y == y {
			if loaf[i].X == x {
				loaf[i] = val
				break
			}
		}
	}

	return loaf

}

//Dough makes and returns an array of Bread with width and height
func Dough(width int, height int) []Bread {
	//ToastLogger("Dough")
	var butt []Bread
	butt = make([]Bread, width*height)

	return butt
}

//DoughMax makes and sets an array of Bread with width and height equal to the
//current terminal dimensions
func DoughMax() (int, int, []Bread, Loaf) {
	//ToastLogger("DoughMax")
	var butt []Bread
	var loaf Loaf
	height, err := terminaldimensions.Height()
	width, err := terminaldimensions.Width()
	if err != nil {
		fmt.Println("Dimensional error")
		//fmt.Println(strconv.Atoi(string(height)))
		//fmt.Println(strconv.Atoi(string(width)))
	}
	loaf.Height = int(height)
	loaf.Width = int(width)
	//fmt.Println("Dough all mooshy!")
	//the nines can be changed
	heightInt := int(height)
	widthInt := int(width)
	butt = make([]Bread, widthInt*heightInt)

	return widthInt, heightInt, butt, loaf

}

//ToastLogger is called with every flour command and gives helpful insight into
//what is currently being run. Activate by setting blab to 1 and uncommenting
//the function calls in all of flour. TODO: Streamline this process.
func ToastLogger(logger string) {
	blab := 0
	loggo, err := os.OpenFile("toast.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	defer loggo.Close()
	_, err = os.Lstat("toast.log")
	if err != nil {
		fmt.Println("Fatal error")
	}
	if blab == 1 {
		loggo.WriteString("===" + logger + "===\n")
	}
}
